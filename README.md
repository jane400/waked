# Waked
Waked is a daemon which lets Apps wake the system from suspend at requested times

# Install
There is a PKGBUILD here: https://gitlab.com/seath1/pkgbuild/-/tree/master/waked
After installing start/enable the service:
```
systemctl enable waked
systemctl start waked
```

# Dependencies
 - sdbus-cpp
 - the usual stuff to build things (cmake, g++, make, ...)

# Usage
The service listens on the system bus at
```
service:   de.seath.Waked
interface: de.seath.Waked
object:    /de/seath/Waked/Alarm
```
Methods:
```
Add   (string id, uint64 time)
Update(string id, uint64 time)
Remove(string id)
```

To add/change/remove an alarm use the corrosponding method via dbus.
 - id can be freely chosen.
 - time is the timestamp to wake up the system in unix time (seconds since epoch)

 # State
 atm just a proof of concept to use a pinephone as alarm clock with a modified gnome-clocks version you can find here: https://gitlab.com/seath1/gnome-clocks-waked

 The following features may or may not be added in the future:
 - use kernel timers or systemd timers or something completly different instead of writing to the rtc
 - persist registered timers between reboots and restarting the service.
 - concept to allow/deny specific Apps
 - change the API without further notice
 - your idea?
